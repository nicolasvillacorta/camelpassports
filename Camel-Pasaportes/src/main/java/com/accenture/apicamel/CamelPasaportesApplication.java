package com.accenture.apicamel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
public class CamelPasaportesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelPasaportesApplication.class, args);
	}

}
