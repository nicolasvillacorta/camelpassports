package com.accenture.apicamel.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.camel.Exchange;

public class Util {
	
	/**
	 * @param metodo Tipo de HTTP_Method.
	 * @param info Datos generados/recuperados del request.
	 * @return Retorna el string que va a ser insertado en el archivo de logs.
	 */
	public static String generarLog(String metodo, String info) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("Fecha y hora: ");
		sb.append(fechaParseada());
		sb.append(", ");
		sb.append("Operacion: ");
		sb.append(metodo);
		sb.append(", ");
		sb.append("Datos: ");
		sb.append(info);
		sb.append(System.getProperty("line.separator"));
		
		return sb.toString();
	}
	/**
	 * @param exchange El exchange del que extraer el header. 
	 * @param param El parametro a buscar en el header.
	 * 
	 * @return El parametro enviado por el header.
	 */
	public static String exchangeParam(Exchange exchange, String param) {
		try {
			return exchange.getIn().getHeader(param).toString();
		}catch(Exception e) {
			return null;
		}
		
	}
	
	/**
	 * @return Nueva fecha con formato "dd/MM/yyyy HH:mm:ss".
	 */
	public static String fechaParseada() {
		
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String fecha = df.format(new Date());
		
		return fecha;
	}

}
