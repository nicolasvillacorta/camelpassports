package com.accenture.apicamel.util;

public interface Constantes {

	/**
	 * Url donde se guardan los logs.
	 */
	public final static String LOGS_PATH = "src/main/resources";
	
	/**
	 * Ruta direct generadora de logs
	 */
	public final static String LOG_FILE = "direct:logFile";
	
}
