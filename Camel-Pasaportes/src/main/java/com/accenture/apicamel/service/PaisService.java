package com.accenture.apicamel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.accenture.apicamel.models.Pais;
import com.accenture.apicamel.repository.PaisDao;

@Service
public interface PaisService extends PaisDao {

	public Pais findByNombre(String nombre);

	public void deleteByNombre(String nombre);

}
