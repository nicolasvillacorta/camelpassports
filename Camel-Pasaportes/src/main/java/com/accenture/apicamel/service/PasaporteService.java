package com.accenture.apicamel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.accenture.apicamel.models.Pasaporte;
import com.accenture.apicamel.repository.PasaporteDao;

@Service
public interface PasaporteService extends PasaporteDao {

	public Pasaporte findByNroPasaporte(String nroPasaporte);
	
}
