package com.accenture.apicamel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.accenture.apicamel.models.Persona;
import com.accenture.apicamel.repository.PersonaDao;

@Service
public interface PersonaService extends PersonaDao {

	public Persona findByNroCedula(String nroCedula);
	public void deleteByNroCedula(String nroCedula);
	
}
