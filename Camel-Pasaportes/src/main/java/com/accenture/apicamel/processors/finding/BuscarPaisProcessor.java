package com.accenture.apicamel.processors.finding;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.exceptions.NoResultsException;
import com.accenture.apicamel.models.Pais;
import com.accenture.apicamel.service.PaisService;
import com.accenture.apicamel.util.Util;

@Component
public class BuscarPaisProcessor implements Processor {

	@Autowired
	PaisService paisService;
	
	public void process(Exchange exchange) throws Exception {
		
		String parametro = Util.exchangeParam(exchange, "nombre");
		
		if(parametro==null) {
			List<Pais> paisesFound = paisService.findAll();
			exchange.getOut().setBody(paisesFound);
		} else{
			Pais paisFound = paisService.findByNombre(parametro);
			
			if(paisFound!=null) 
				exchange.getOut().setBody(paisFound);
			else
				throw new NoResultsException();
			
		}
			
		
		
		
//		if(parametro==null) {
//			List<Pais> paisesFound = paisService.findAll();
//			System.out.println("ASDQWEQWEQWEQWE");
//			exchange.getOut().setBody(paisesFound);
//		}else {
//			Pais paisFound = paisService.findByNombre(parametro);
//			exchange.getOut().setBody(paisFound);
//		}
			
		
		
//		if(exchange.getIn().getHeader("nombre")==null) {
//			paisesFound = paisService.findAll();
//			exchange.getOut().setBody(paisesFound);
//		}else {
//			paisFound = paisService.findByNombre(Util.exchangeParam(exchange, "nombre"));
//			exchange.getOut().setBody(paisFound);
//		}
		
		
	}
	
}
