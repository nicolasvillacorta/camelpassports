package com.accenture.apicamel.processors.updates;

import java.util.ArrayList;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.exceptions.NoResultsException;
import com.accenture.apicamel.models.Pais;
import com.accenture.apicamel.models.Persona;
import com.accenture.apicamel.models.dto.BlackListDto;
import com.accenture.apicamel.service.PaisService;
import com.accenture.apicamel.service.PersonaService;

@Component
public class AddBlackListProcessor implements Processor {

	@Autowired
	PersonaService personaService;
	@Autowired
	PaisService paisService;
	
	@Override
	public void process(Exchange exchange) throws Exception {

		BlackListDto dto = exchange.getIn().getBody(BlackListDto.class);
		
		Pais pais = paisService.findByNombre(dto.getNombrePais());
		Persona persona = personaService.findByNroCedula(dto.getCedulaDelAspirante());
		
		if(pais==null || persona==null)
			throw new NoResultsException();
		
		if(pais.getListaNegra()==null) {
			pais.setListaNegra(new ArrayList<Persona>());
		}
		
		
		
		pais.addListaNegra(persona);
		paisService.save(pais);
		
		exchange.getOut().setBody(pais);
	}

}
