package com.accenture.apicamel.processors.creation;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.models.Pais;
import com.accenture.apicamel.service.PaisService;

@Component
public class CreatePaisProcessor implements Processor {

	@Autowired
	PaisService paisService;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		paisService.save(exchange.getIn().getBody(Pais.class));
		
	}

	
	
}
