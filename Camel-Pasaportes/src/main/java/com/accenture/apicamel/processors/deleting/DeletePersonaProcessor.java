package com.accenture.apicamel.processors.deleting;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.service.PersonaService;

@Component
public class DeletePersonaProcessor implements Processor{

	@Autowired
	PersonaService personaService;
	
	public void process(Exchange exchange) throws Exception {
		personaService.deleteByNroCedula(exchange.getIn().getHeader("cedula").toString());
	}
	
}
