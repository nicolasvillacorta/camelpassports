package com.accenture.apicamel.processors.deleting;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.exceptions.NoResultsException;
import com.accenture.apicamel.models.Pais;
import com.accenture.apicamel.service.PaisService;
import com.accenture.apicamel.util.Util;

@Component
public class DeletePaisProcessor implements Processor {
	
	@Autowired
	PaisService paisService;
	
	public void process(Exchange exchange) throws Exception {
		
		Pais paisBuscado = paisService.findByNombre(Util.exchangeParam(exchange, "nombre"));
		
		if(paisBuscado!=null)
			exchange.getOut().setBody(paisBuscado);
		else
			throw new NoResultsException();
	}

}
