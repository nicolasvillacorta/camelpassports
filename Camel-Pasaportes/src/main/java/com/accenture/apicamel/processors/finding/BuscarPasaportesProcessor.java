package com.accenture.apicamel.processors.finding;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.exceptions.NoResultsException;
import com.accenture.apicamel.models.Pasaporte;
import com.accenture.apicamel.service.PasaporteService;
import com.accenture.apicamel.util.Util;

@Component
public class BuscarPasaportesProcessor implements Processor {

	@Autowired
	PasaporteService pasaporteService;
	
	@Override
	public void process(Exchange exchange) throws Exception {

		Pasaporte pasaporteFound;
		List<Pasaporte> pasaportesFound;
		
		if(exchange.getIn().getHeader("nro")==null) {
			pasaportesFound = pasaporteService.findAll();
			exchange.getOut().setBody(pasaportesFound);
		}
		else {
			pasaporteFound = pasaporteService.findByNroPasaporte(Util.exchangeParam(exchange, "nro"));
			exchange.getOut().setBody(pasaporteFound);
		}
	
	}

}
