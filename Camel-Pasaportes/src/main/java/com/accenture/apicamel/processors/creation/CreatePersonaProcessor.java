package com.accenture.apicamel.processors.creation;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.models.Persona;
import com.accenture.apicamel.service.PersonaService;

@Component
public class CreatePersonaProcessor implements Processor {

	@Autowired
	PersonaService personaService;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		personaService.save(exchange.getIn().getBody(Persona.class));
	}

}
