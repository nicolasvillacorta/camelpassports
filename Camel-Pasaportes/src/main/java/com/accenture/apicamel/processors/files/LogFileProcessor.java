package com.accenture.apicamel.processors.files;

import javax.servlet.http.HttpServletRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.util.Util;

@Component
public class LogFileProcessor implements Processor {

	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		String metodo = exchange.getIn().getHeader(Exchange.HTTP_METHOD).toString();
		String info = exchange.getIn().getBody().toString();
		
		exchange.getOut().setBody(Util.generarLog(metodo, info));
	}

}
