package com.accenture.apicamel.processors.updates;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.models.Pasaporte;
import com.accenture.apicamel.models.Persona;
import com.accenture.apicamel.models.dto.PasaporteDto;
import com.accenture.apicamel.service.PasaporteService;
import com.accenture.apicamel.service.PersonaService;
import com.accenture.apicamel.util.Util;

@Component
public class NewOwnerProcessor implements Processor{

	@Autowired
	PersonaService personaService;
	@Autowired
	PasaporteService pasaporteService;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		PasaporteDto dto = exchange.getIn().getBody(PasaporteDto.class);
		
		Pasaporte pasaporte = pasaporteService.findByNroPasaporte(dto.getNroPasaporte());
		Persona newOwner = personaService.findByNroCedula(dto.getCedulaOwner());

		pasaporte.setOwner(newOwner);
		pasaporte.setFechaExpedicion(Util.fechaParseada());
		pasaporteService.save(pasaporte);
		
		exchange.getOut().setBody(pasaporte);
	}

}
