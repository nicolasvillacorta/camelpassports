package com.accenture.apicamel.processors.finding;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.exceptions.NoResultsException;
import com.accenture.apicamel.models.Pasaporte;
import com.accenture.apicamel.models.Persona;
import com.accenture.apicamel.service.PersonaService;
import com.accenture.apicamel.util.Util;

@Component
public class PersonaByCedulaProcessor implements Processor{
	
	@Autowired
	PersonaService personaService;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		Persona personaFound;
		List<Persona> personasFound;
		
		if(exchange.getIn().getHeader("cedula")==null) {
			personasFound = personaService.findAll();
			exchange.getOut().setBody(personasFound);
		}
		else {
			personaFound = personaService.findByNroCedula(Util.exchangeParam(exchange, "cedula"));
			exchange.getOut().setBody(personaFound);
		}
		
	}
	
}
