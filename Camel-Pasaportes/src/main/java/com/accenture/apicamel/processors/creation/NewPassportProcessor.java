package com.accenture.apicamel.processors.creation;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.exceptions.ListaNegraException;
import com.accenture.apicamel.exceptions.NoResultsException;
import com.accenture.apicamel.models.Pais;
import com.accenture.apicamel.models.Pasaporte;
import com.accenture.apicamel.models.Persona;
import com.accenture.apicamel.models.dto.PasaporteDto;
import com.accenture.apicamel.service.PaisService;
import com.accenture.apicamel.service.PasaporteService;
import com.accenture.apicamel.service.PersonaService;
import com.accenture.apicamel.util.Util;

@Component
public class NewPassportProcessor implements Processor{

	@Autowired
	PasaporteService pasaporteService;
	@Autowired
	PersonaService personaService;
	@Autowired
	PaisService paisService;
	
	@Override
	public void process(Exchange exchange) throws Exception {

		PasaporteDto dto = exchange.getIn().getBody(PasaporteDto.class);
		
		Persona owner = personaService.findByNroCedula(dto.getCedulaOwner());
		Pais pais = paisService.findByNombre(dto.getPais());
		
		if(pais!= null && owner !=null) {
			if(pais.getListaNegra().contains(owner)) {
				throw new ListaNegraException();
			}else {
				Pasaporte pasaporteNuevo = new Pasaporte();
				pasaporteNuevo.setFechaExpedicion(Util.fechaParseada());
				pasaporteNuevo.setNroPasaporte(dto.getNroPasaporte());
				pasaporteNuevo.setOwner(owner);
				pasaporteNuevo.setPais(dto.getPais());
				
				pasaporteService.save(pasaporteNuevo);
			}
		}else {
			throw new NoResultsException();
		}
		
		
		
		
//		Persona persona = personaService.findByNroCedula()
//		private String nroPasaporte;
//		private Persona owner;
//		private String fechaExpedicion;
//		private String pais;
		
	}

}
