package com.accenture.apicamel.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.accenture.apicamel.models.Pasaporte;

@Repository
public interface PasaporteDao extends MongoRepository<Pasaporte, String>{

}
