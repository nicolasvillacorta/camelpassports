package com.accenture.apicamel.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.accenture.apicamel.models.Pais;

@Repository
public interface PaisDao extends MongoRepository<Pais, String>{

}
