package com.accenture.apicamel.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.accenture.apicamel.models.Persona;

@Repository
public interface PersonaDao extends MongoRepository<Persona, String> {

}
