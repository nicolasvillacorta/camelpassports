package com.accenture.apicamel.models.dto;

import java.io.Serializable;

import com.google.gson.Gson;

public class PasaporteDto {
	private String nroPasaporte;
	private String cedulaOwner;
	private String pais;
	private String prioridad;

	public String getNroPasaporte() {
		return nroPasaporte;
	}

	public void setNroPasaporte(String nroPasaporte) {
		this.nroPasaporte = nroPasaporte;
	}

	public String getCedulaOwner() {
		return cedulaOwner;
	}

	public void setCedulaOwner(String cedulaOwner) {
		this.cedulaOwner = cedulaOwner;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

}
