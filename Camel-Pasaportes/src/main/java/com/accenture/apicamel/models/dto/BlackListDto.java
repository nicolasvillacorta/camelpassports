package com.accenture.apicamel.models.dto;

public class BlackListDto {

	private String nombrePais;
	private String cedulaDelAspirante;

	public String getNombrePais() {
		return nombrePais;
	}

	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}

	public String getCedulaDelAspirante() {
		return cedulaDelAspirante;
	}

	public void setCedulaDelAspirante(String cedulaDelAspirante) {
		this.cedulaDelAspirante = cedulaDelAspirante;
	}

	@Override
	public String toString() {
		return "BlackListDto [nombrePais=" + nombrePais + ", cedulaDelAspirante=" + cedulaDelAspirante + "]";
	}

}
