package com.accenture.apicamel.controllers;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.models.Pasaporte;
import com.accenture.apicamel.models.dto.PasaporteDto;
import com.accenture.apicamel.processors.creation.NewPassportProcessor;
import com.accenture.apicamel.processors.finding.BuscarPasaportesProcessor;
import com.accenture.apicamel.processors.updates.NewOwnerProcessor;
import com.accenture.apicamel.util.Constantes;

@Component
public class PasaporteController extends RouteBuilder {

	private final String LOG_FILE = Constantes.LOG_FILE;
	
	@Autowired
	BuscarPasaportesProcessor buscarPasaportesProcessor;
	@Autowired
	NewOwnerProcessor newOwnerProcessor;
	@Autowired
	NewPassportProcessor newPassportProcessor;
	
	public void configure() throws Exception {
		
		restConfiguration().component("servlet").port(9090).bindingMode(RestBindingMode.json);
		
		rest("/pasaporte")
			.get().route().process(buscarPasaportesProcessor)
				.setHeader(Exchange.HTTP_METHOD, simple("GET")).wireTap(LOG_FILE).end().endRest()
			.get("/{nro}").route().process(buscarPasaportesProcessor)
				.setHeader(Exchange.HTTP_METHOD, simple("GET")).wireTap(LOG_FILE).end().endRest()
			.put().type(PasaporteDto.class).outType(Pasaporte.class).route().process(newOwnerProcessor)
				.setHeader(Exchange.HTTP_METHOD, simple("PUT")).wireTap(LOG_FILE).end().endRest()
			// Si el pasaporte o el pais ingresados no existen, tira NoResultsExceptions, si existen ambos, pero la persona esta en la lista negra del pais tira
			// ListaNegraException, de otra manera, el pasaporte es agregado a la BD con su owner. Si la prioridad es 1, se realiza todo inmediatamente, si la prioridad
			// es 0, se mete en una cola para esperar ser consumida (Aun no realizado el consumo)
			.post().type(PasaporteDto.class).outType(Pasaporte.class).route() 
				.choice()
					.when().simple("${body.prioridad} == 0").marshal().json(JsonLibrary.Jackson, PasaporteDto.class).wireTap("activemq:pending_passports")
						.unmarshal().json(JsonLibrary.Jackson, PasaporteDto.class).endChoice()
					.when().simple("${body.prioridad} == 1").process(newPassportProcessor)
					.otherwise().to("direct:error").endRest();

	}
	
}
