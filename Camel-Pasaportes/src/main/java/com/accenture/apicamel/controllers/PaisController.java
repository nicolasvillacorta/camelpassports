package com.accenture.apicamel.controllers;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.exceptions.NoResultsException;
import com.accenture.apicamel.models.Pais;
import com.accenture.apicamel.models.dto.BlackListDto;
import com.accenture.apicamel.processors.creation.CreatePaisProcessor;
import com.accenture.apicamel.processors.deleting.DeletePaisProcessor;
import com.accenture.apicamel.processors.finding.BuscarPaisProcessor;
import com.accenture.apicamel.processors.updates.AddBlackListProcessor;
import com.accenture.apicamel.util.Constantes;

@Component
public class PaisController extends RouteBuilder{

	private final String LOG_FILE = Constantes.LOG_FILE;
	
	@Autowired
	CreatePaisProcessor crearPaisProcessor;
	@Autowired
	BuscarPaisProcessor buscarPaisProcessor;
	@Autowired
	DeletePaisProcessor deletePaisProcessor;
	@Autowired
	AddBlackListProcessor addBlackListProcessor;
	
	@Override
	public void configure() throws Exception {
		
		restConfiguration().component("servlet").host("localhost").port(9090).bindingMode(RestBindingMode.json)
		.apiContextPath("/swagger").apiContextRouteId("swagger").contextPath("").apiProperty("api.title", "cuenta")
		.apiProperty("api.version", "1.0");
		
		rest("/pais")
			.get().route().process(buscarPaisProcessor).setHeader(Exchange.HTTP_METHOD, simple("GET"))
				.wireTap(LOG_FILE).end().endRest()
			.get("/{nombre}").route()
				.doTry()
					.process(buscarPaisProcessor)
				.doCatch(NoResultsException.class)
					.to("direct:noResults")
				.doFinally()
					.setHeader(Exchange.HTTP_METHOD, simple("GET")).wireTap(LOG_FILE).delay(1)
			.end().endRest()
			
			.post().type(Pais.class).route().process(crearPaisProcessor)
				.setHeader(Exchange.HTTP_METHOD, simple("POST")).wireTap(LOG_FILE).end().endRest()
				
			.delete("/{nombre}").route()
				.doTry()
					.process(deletePaisProcessor)
				.doCatch(NoResultsException.class)
					.to("direct:noResults")
				.doFinally()
				.setHeader(Exchange.HTTP_METHOD, simple("DELETE")).wireTap(LOG_FILE).delay(1)
			.end().endRest()
			
			.put().type(BlackListDto.class).outType(Pais.class).route().process(addBlackListProcessor)
				.setHeader(Exchange.HTTP_METHOD, simple("PUT")).wireTap(LOG_FILE).end().endRest();
		
	}

}