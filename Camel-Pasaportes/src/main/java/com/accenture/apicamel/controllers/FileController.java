package com.accenture.apicamel.controllers;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.processors.files.LogFileProcessor;
import com.accenture.apicamel.util.Constantes;

@Component
public class FileController extends RouteBuilder {

	private final String Path = Constantes.LOGS_PATH;
	
	@Autowired
	LogFileProcessor logFileProcessor;
	
	@Override
	public void configure() throws Exception {

		from("direct:logFile").process(logFileProcessor).to("file:"+Path+"?fileName=logs.log&fileExist=Append");

	}

}
