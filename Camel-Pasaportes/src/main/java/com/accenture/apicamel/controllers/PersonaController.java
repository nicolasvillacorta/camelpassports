package com.accenture.apicamel.controllers;


import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.apicamel.models.Pais;
import com.accenture.apicamel.models.Persona;
import com.accenture.apicamel.processors.creation.CreatePersonaProcessor;
import com.accenture.apicamel.processors.deleting.DeletePersonaProcessor;
import com.accenture.apicamel.processors.finding.PersonaByCedulaProcessor;
import com.accenture.apicamel.util.Constantes;

@Component
public class PersonaController extends RouteBuilder {

	private final String LOG_FILE = Constantes.LOG_FILE;
	
	@Autowired
	CreatePersonaProcessor crearPersonaProcessor;
	@Autowired
	PersonaByCedulaProcessor personaByCedulaProcessor;
	@Autowired
	DeletePersonaProcessor deletePersonaProcessor;
	
	@Override
	public void configure() throws Exception {
		
		restConfiguration().component("servlet").port(9090).bindingMode(RestBindingMode.json);

		rest("/persona")
			.get().route().process(personaByCedulaProcessor)
				.setHeader(Exchange.HTTP_METHOD, simple("GET")).wireTap(LOG_FILE).end().endRest()
			.get("/{cedula}").route().process(personaByCedulaProcessor)
				.setHeader(Exchange.HTTP_METHOD, simple("GET")).wireTap(LOG_FILE).end().endRest()
			.post().type(Persona.class).route()
					.split(simple("${body}"))
						.process(crearPersonaProcessor)
						.setHeader(Exchange.HTTP_METHOD, simple("POST")).wireTap(LOG_FILE).end().endRest()	
			.delete("/{cedula}").route().process(deletePersonaProcessor)
				.setHeader(Exchange.HTTP_METHOD, simple("DELETE")).wireTap(LOG_FILE).end().endRest();
				
	}
	
}
