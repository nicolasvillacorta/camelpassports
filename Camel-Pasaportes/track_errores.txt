
1)	ClassNotFoundException.javax.servlet.http.HttpServletResponse
	1.1		Lo solucione, agregar la sig dependencia:
					<dependency>
						<groupId>javax.servlet</groupId>
						<artifactId>servlet-api</artifactId>
						<version>2.5</version>
					</dependency>

	1.2		Seguia con este servlet, la app corria y se cerraba inmediatamente.
			Todo este problema fue porque no habia agregado la dependencia de springboot web, despues de agregarla saque el javax.servlet
		
2)	Entity must not be null al hacer POST.
	Solucion, agregarle a la ruta "type(nombreClase.class)", el "outType(...)" no es necesario.
	
3)  El GET no retorna el resultado del findByNroCedula, si no se setea en el body del exchange,
		exchange.getOut().setBody(resultado) <-- En el processor.
		
4)  No podia hacer funcionar el broken de mensajes.
	Solucion, use en el routeo "activemq:...." en lugar de "jms:queue:..." y me pasaron las 3 dependencias correctas.
	
5)  No podia enviar le contenido del JSon que entraba al post al ActiveMQ.
	Solucion, marshal().json(...).wireTap("activemq:...") <-- En lugar de hacer un to, hice un wireTap.
	
6) 	No podia encontrar con el "paises.contains(persona)" a la persona dentro de la lista, me tiraba siempre false.
	Solucion, no habia overrideado ni equals ni hashcode